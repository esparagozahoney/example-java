package src.javaapplication4;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Grades{
    static int ans1,ans2,answerMain,back,exit,mathWW1,mathWW2,mathWW3,mathWW4,mathWW5,mathWW6,mathWW7,mathWW8,mathWW9,mathWW10,QA1Score,QA1Assess;
    static int wwcounter1=0,wwcounter2=0,wwcounter3=0,wwcounter4=0,wwcounter5=0,wwcounter6=0,wwcounter7=0,wwcounter8=0,wwcounter9=0,wwcounter10=0;
    static int ansQA1,wwtotal,ptTotal,qa1total;
    static double total,ptTot;
    static double wwPs,wwWs,ptPs,ptws,qa1wg,x,y,z;
    static int mathpt1,mathpt2,mathpt3,mathpt4,mathpt5,mathpt6,mathpt7,mathpt8,mathpt9,mathpt10;
    static int ptcounter1=0, ptcounter2=0,ptcounter3=0,ptcounter4=0,ptcounter5=0,ptcounter6=0,ptcounter7=0,ptcounter8=0,ptcounter9=0,ptcounter10=0;



    static Scanner input = new Scanner (System.in);
    public static void main (String[]args){


        do{
            mathMainMenu ();

            switch(answerMain){
                case 1:
                    do{
                        mathWw ();
                        back ();
                    }while(back==1);
                    break;

                case 2:
                    do{
                        mathPerformance();
                        back ();
                    }while(back==1);
                    break;

                case 3:
                    QA1 ();
                    break;

                case 4:
                    do{
                        x=perf();
                        System.out.println("The weighted score of PT is "+x);
                        y=ww();
                        System.out.println("The weighted score of WW is "+y);
                        z=QA ();
                        System.out.println("The weighted score of QA is "+z);
                        back ();
                    }while(back==1);
                    break;

                default:
                    System.out.println("Invalid input");
                    break;
            }
            back ();

        } while(back==2);
    }
    static void mathWw (){


        System.out.println("                         Select Written Works to put Score:            ");
        System.out.println("|---------------------------------------------------------------------|");
        System.out.println("|1-WW1   3-WW3       5-WW5      7-WW7     9-WW9      11-Score         |");
        System.out.println("|2-WW2   4-WW4       6-WW6      8-WW98   10-WW10     12-Exit          |");
        System.out.println("|---------------------------------------------------------------------|");

        ans1 = getIntegerMenuResponse(input);

        switch(ans1){
            case 1:
                if(wwcounter1==1){
                    System.out.println("You already put score in W1");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 1");
                        mathWW1 = getIntegerMenuResponse(input);
                        if(mathWW1<11){
                            if(mathWW1>0){
                                wwcounter1=wwcounter1+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW1>10);
                }
                break;

            case 2:
                if(wwcounter2==1){
                    System.out.println("You already put score in W2");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 2");
                        mathWW2 = getIntegerMenuResponse(input);
                        if(mathWW2<11){
                            if(mathWW2>0){
                                wwcounter2=wwcounter2+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW2>10);
                }
                break;
            case 3:
                if(wwcounter3==1){
                    System.out.println("You already put score in W3");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 3");
                        mathWW3 = getIntegerMenuResponse(input);
                        if(mathWW3<11){
                            if(mathWW3>0){
                                wwcounter3=wwcounter3+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW3>10);
                }
                break;
            case 4:
                if(wwcounter4==1){
                    System.out.println("You already put score in W4");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 4");
                        mathWW4 = getIntegerMenuResponse(input);
                        if(mathWW4<11){
                            if(mathWW4>0){
                                wwcounter4=wwcounter4+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW4>10);
                }
                break;
            case 5:
                if(wwcounter5==1){
                    System.out.println("You already put score in W5");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 5");
                        mathWW5 = getIntegerMenuResponse(input);
                        if(mathWW5<11){
                            if(mathWW5>0){
                                wwcounter5=wwcounter5+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW5>10);
                }
                break;
            case 6:
                if(wwcounter6==1){
                    System.out.println("You already put score in W6");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 6");
                        mathWW6 = getIntegerMenuResponse(input);
                        if(mathWW6<11){
                            if(mathWW6>0){
                                wwcounter6=wwcounter6+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW6>10);
                }
                break;
            case 7:
                if(wwcounter7==1){
                    System.out.println("You already put score in W7");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 7");
                        mathWW7 = getIntegerMenuResponse(input);
                        if(mathWW7<11){
                            if(mathWW7>0){
                                wwcounter7=wwcounter7+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW7>10);
                }
                break;
            case 8:
                if(wwcounter8==1){
                    System.out.println("You already put score in W8");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 8");
                        mathWW8 = getIntegerMenuResponse(input);
                        if(mathWW8<11){
                            if(mathWW8>0){
                                wwcounter8=wwcounter8+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW8>10);
                }
                break;
            case 9:
                if(wwcounter9==1){
                    System.out.println("You already put score in W9");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 9");
                        mathWW9 = getIntegerMenuResponse(input);
                        if(mathWW9<11){
                            if(mathWW9>0){
                                wwcounter9=wwcounter9+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW9>10);
                }
                break;
            case 10:
                if(wwcounter10==1){
                    System.out.println("You already put score in W10");
                }else{
                    do{
                        System.out.println("Please input score in Written Works 10");
                        mathWW10 = getIntegerMenuResponse(input);
                        if(mathWW10<11){
                            if(mathWW10>0){
                                wwcounter10=wwcounter10+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathWW10>10);
                }
                break;
            case 11:
                wwtotal ();
                break;
            case 12:
                mathMainMenu ();
                break;
        }}

    static void mathMainMenu (){

        System.out.println("                            First Quarter                                                ");
        System.out.println("                       Please choose from the menu :                                     ");
        System.out.println("|---------------------------------------------------------------------------------------|");
        System.out.println("|1-Written Works  2-Performance task  3-Quarterly Assessment  4-Grade   5-Exit           |");
        System.out.println("|---------------------------------------------------------------------------------------|");

        getIntegerMenuResponse(input);
    }

    static boolean checkForInt(Scanner input) {
        if(input.hasNextInt()) {
            System.out.println("Has got int-like");
            answerMain = input.nextInt();
            return false;
        } else {
            System.out.println("Not int like thing");
            // get the value entered by user and disregard it. Ask for inputting integer.
            String dev_null = input.next();
            System.out.println("Please choose number as an input");
            return true;
        }
    }

    static int getIntegerMenuResponse (Scanner input) {
        boolean is_not_integer_response = true;
        do {
            try {
                answerMain = input.nextInt();
                is_not_integer_response = false;
            } catch(InputMismatchException ex) {
                System.out.println("You didn't enter a number. You need to enter a number to proceed");
                is_not_integer_response = checkForInt(input);
            }
        } while(is_not_integer_response);

        return answerMain;
    }
    static void back (){
        System.out.println("-------------------------------------------------------|");
        System.out.println("Press 1-Back    2-Back to main menu   3-End Program    |");
        System.out.println("-------------------------------------------------------|");
        back = getIntegerMenuResponse(input);
    }

    static void mathPerformance() {

        System.out.println("                  Select Performance Task to put Score:     ");
        System.out.println("|-------------------------------------------------------------|");
        System.out.println ("|1-PT1   3-PT3      5-PT5       7-PT7    9-PT9       11-Score |");
        System.out.println ("|2-PT2   4-PT4      6-PT6       8-PT8   10-PT10     12-Exit   |");
        System.out.println ("|-------------------------------------------------------------|");
        ans2 = getIntegerMenuResponse(input);

        switch(ans2){
            case 1:
                if(ptcounter1==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score in Performance Task 1");
                        mathpt1 = getIntegerMenuResponse(input);
                        if(mathpt1<11){
                            if(mathpt1>0){
                                ptcounter1=+1;
                            }
                        }else{
                            System.out.println("Please input 1-10 score only");
                        }
                    }while(mathpt1>10);
                }
                break;

            case 2:
                if(ptcounter2==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 2");
                        mathpt2 = getIntegerMenuResponse(input);
                        if (mathpt2<11){
                            if(ptcounter2>0){
                                ptcounter2+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt2>10);
                }
                break;
            case 3:
                if(ptcounter3==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 3");
                        mathpt3 = getIntegerMenuResponse(input);

                        if (mathpt3<11){
                            if(ptcounter3<0){
                                ptcounter3+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt3>10);
                }
                break;
            case 4:
                if(ptcounter4==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 4");
                        mathpt4 = getIntegerMenuResponse(input);

                        if (mathpt4<11){
                            if(ptcounter4>0){
                                ptcounter4+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt4>10);
                }
                break;
            case 5:
                if(ptcounter5==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 5");
                        mathpt1 = getIntegerMenuResponse(input);

                        if (mathpt5<11){
                            if(ptcounter5>0){
                                ptcounter5+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt5>10);
                }
                break;
            case 6:
                if(ptcounter6==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 6");
                        mathpt6 = getIntegerMenuResponse(input);

                        if (mathpt6<11){
                            if(ptcounter6>0){
                                ptcounter6+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt6>10);
                }
                break;
            case 7:
                if(ptcounter7==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 7");
                        mathpt7 = getIntegerMenuResponse(input);

                        if (mathpt1<11){
                            if(ptcounter7>0){
                                ptcounter7+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt7>10);
                }
                break;
            case 8:
                if(ptcounter8==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 8");
                        mathpt8 = getIntegerMenuResponse(input);

                        if (mathpt8<11){
                            if(ptcounter8>0){
                                ptcounter8+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt8>10);
                }
                break;
            case 9:
                if(ptcounter9==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 9");
                        mathpt9 = getIntegerMenuResponse(input);

                        if (mathpt9<11){
                            if(ptcounter9>0){
                                ptcounter9+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt9>10);
                }
                break;
            case 10:
                if(ptcounter10==1){
                    System.out.println("You already put score in PT");
                }else{
                    do{
                        System.out.println("Please input score for Performance task 10");
                        mathpt10 = getIntegerMenuResponse(input);

                        if (mathpt10<11){
                            if(ptcounter10>0){
                                ptcounter10+=1;
                            }
                        }else{
                            System.out.println("Please enter score 1-10");
                        }
                    } while(mathpt10>10);
                }
                break;
            case 11:
                ptTotal ();
        }}
    static void QA1(){
        System.out.println("                Select Menu                                       ");
        System.out.println("|---------------------------------------------------------------|");
        System.out.println ("|1-Quarterly Assessment  2-Quarterly Assessment score     3-exit  |");
        System.out.println ("|---------------------------------------------------------------|");
        ansQA1 = getIntegerMenuResponse(input);

        switch(ansQA1){
            case 1:
                int ans,ans2,ans3,qaCounter=0,qaCounter1=0;
                if(qaCounter==1){
                    System.out.println("You already put Assessment Item in First Quarter");
                }else{
                    do{
                        System.out.println("Please Enter the Assessment Items");
                        QA1Assess = getIntegerMenuResponse(input);
                        System.out.println("Confirm Assessment Item?Press 1-Yes 2-No");
                        ans = getIntegerMenuResponse(input);

                    }while(ans==1);
                    qaCounter=qaCounter+1;
                }
                break;
            case 2:
                System.out.println("The student's First Quarter Assessment Score is: ");
                break;
            case 3:

                break;
            default:
                System.out.println("Invalid Menu");
                break;
        }}
    static void wwtotal(){
        System.out.println ("The score of WW1  is: "+ mathWW1);
        System.out.println ("The score of WW2  is: "+ mathWW2);
        System.out.println("The score of WW3  is: "+ mathWW3);
        System.out.println("The score of WW4  is: "+ mathWW4);
        System.out.println("The score of WW5  is: "+ mathWW5);
        System.out.println("The score of WW6  is: "+ mathWW6);
        System.out.println("The score of WW7  is: "+ mathWW7);
        System.out.println ("The score of WW8  is: "+ mathWW8);
        System.out.println("The score of WW9  is: "+ mathWW9);
        System.out.println("The score of WW10 is: "+ mathWW10);

        total=(mathWW1+mathWW2+mathWW3+mathWW4+mathWW5+mathWW6+mathWW7+mathWW8+mathWW9+mathWW10);
        System.out.println("This is the total of Written works = "+ total);
        wwPs=(total/100)*100;
        wwWs=(wwPs*.40);
        System.out.println("Total score :"+total+ "  = \t"+wwPs+"%");
        System.out.println("Weighted Score is "+wwWs);
    }
    static void ptTotal(){
        System.out.println("The student's Performance score\n");
        System.out.println("Performance task 1  "+ mathpt1);
        System.out.println("Performance task 2  "+ mathpt2);
        System.out.println("Performance task 3  "+ mathpt3);
        System.out.println("Performance task 4  "+ mathpt4);
        System.out.println("Performance task 5  "+ mathpt5);
        System.out.println("Performance task 6  "+ mathpt6);
        System.out.println("Performance task 7  "+ mathpt7);
        System.out.println("Performance task 8  "+ mathpt8);
        System.out.println("Performance task 9  "+ mathpt9);
        System.out.println("Performance task 10 "+ mathpt10);
        ptTot=mathpt1+mathpt2+mathpt3+mathpt4+mathpt5+mathpt6+mathpt7+mathpt8+mathpt9+mathpt10;
        ptPs=(ptTot/100)*100;
        ptws=(ptPs*0.40);
        System.out.println("Total score: "+ptTot+"=\t"+ptPs);
        System.out.println("Weighted Score is "+ptws);
    }
    static void qa1total (){
        qa1total=(QA1Score/QA1Assess)*100;
        qa1wg=qa1total*0.20;
        System.out.println("Student score "+QA1Score+"/"+QA1Assess+"=\t"+qa1total+"%");

        System.out.println("Weighted Score is "+qa1wg);
    }

    static double perf(){
        ptTot=mathpt1+mathpt2+mathpt3+mathpt4+mathpt5+mathpt6+mathpt7+mathpt8+mathpt9+mathpt10;
        ptPs=(ptTot/100)*100;
        ptws=(ptPs*0.40);

        return ptws;
    }
    static double ww(){
        total=(mathWW1+mathWW2+mathWW3+mathWW4+mathWW5+mathWW6+mathWW7+mathWW8+mathWW9+mathWW10);
        System.out.println("This is the total of Written works = "+ total);
        wwPs=(total/100)*100;
        wwWs=(wwPs*.40);

        return wwWs;
    }
    static double QA(){
        qa1total=(QA1Score/QA1Assess)*100;
        qa1wg=(qa1total*0.20);

        return qa1wg;
    }
}