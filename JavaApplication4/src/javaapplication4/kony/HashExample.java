package javaapplication4.kony;

import java.util.HashMap;

public class HashExample {

    public static void main (String [] args) {
        HashMap<String, String> fruits = new HashMap<String, String>();
        fruits.put("yellow", "Lemon");
        fruits.put("red", "Tomatoe");
        fruits.put("green", "Mango");

        System.out.format("You have %d fruits", fruits.size());

        System.out.println("\nRed fruit is: " + fruits.get("red"));

        System.out.println("Do we have green fruit?: " + fruits.containsKey("green"));
        System.out.println("Do we have black fruit?: " + fruits.containsKey("black"));

        System.out.println("displaying all fruits");
        for(String fruit : fruits.values()) {
            System.out.println("frut is: " + fruit);
        }

    }
}
