package javaapplication4.kony;

/**
 *
 * @author kony
 */
import java.util.Scanner;
import java.util.HashMap;

public class Project {

    static HashMap<Integer, Integer> written_works = new HashMap<Integer, Integer>();
    static HashMap<Integer, Integer> performance_tasks = new HashMap<Integer, Integer>();
    static Scanner input = new Scanner (System.in);

    static int ans, back, score;
    static int qa_score = 0;
    static String show_menu;

    public static void main (String args []){
        display_main_menu();
    }
    static void display_main_menu(){

        System.out.println ("_________________________________________");
        System.out.println ("           AMA LOG-IN FORM               ");
        System.out.println (" Enter username : ");
        String username=input.nextLine();
        System.out.println (" Enter password : ");
        int password = input.nextInt();


        if (username.equals("admin") && password == 123456){
            show_assessments_menu();
        }

    }
    static void back (){
        System.out.println("-------------------------------------------------------|");
        System.out.println("Press 1-Back    2-Back to main menu   3-End Program    |");
        System.out.println("-------------------------------------------------------|");
        back = input.nextInt();
        if(back==3) {
            ans = 6;
        }
    }

    static void show_assessments_menu(){

        do {
            System.out.println("                            First Quarter                                                ");
            System.out.println("                       Please choose from the menu :                                     ");
            System.out.println("|---------------------------------------------------------------------------------------|");
            System.out.println("|1-Written Works  2-Performance task  3-Quarterly Assessment  4-Grade 5-Save    6-Exit  |");
            System.out.println("|---------------------------------------------------------------------------------------|");

            ans = input.nextInt ();

            switch (ans){
                case 1:
                    do {
                        show_menu = "written_works";
                        show_written_works_menu();
                    } while(show_menu.equals("written_works"));
                    break;
                case 2:
                    do {
                        show_menu = "performance_tasks";
                        show_performance_task_menu();
                    } while (show_menu.equals("performance_tasks"));
                    break;
                case 3 :
                    do {
                        show_menu = "qa";
                        show_quarterly_assessment_menu();
                    } while (show_menu.equals("qa"));
                    break;
                case 4:
                    do {
                        show_menu = "final_score";
                        show_final_score();
                    } while (show_menu.equals("final_score"));
            }

        } while (ans != 6);

    }

    static int get_total_assessment_map_score(HashMap<Integer, Integer> assessment_map) {
        int total = 0;
        for (Integer score : assessment_map.values()) {
            total = total + score;
        }
        return total;
    }

    static void process_menu (HashMap<Integer, Integer> assessment_map, String assessment_name, int assessment_number, double weight) {
        if (ans >= 1 && ans <= 10) {
            if(assessment_map.containsKey(assessment_number)) {
                System.out.println("You already put score in " + assessment_name + ": " + assessment_number);
            } else {
                do{
                    System.out.println("Please input score for " + assessment_name + " " + assessment_number);
                    score = input.nextInt();
                    if(score <11){
                        assessment_map.put(assessment_number, score);
                        System.out.println("Score " + score + " added to " + assessment_name);
                    }else{
                        System.out.println("Please input 1-10 score only");
                    }
                } while(score >10);
            }
        } else if(ans == 11 ){
            if (assessment_map.size() < 10) {
                for(Integer work : assessment_map.keySet()) {
                    System.out.println(assessment_name + ": " + work + " score is: " + assessment_map.get(work));
                }
                System.out.println("\nAdd all ten " + assessment_name  + " before score can be calculated");
            } else {

                int total = get_total_assessment_map_score(assessment_map);
                double percentage_score = (double) total;
                double weighted_score = (percentage_score * weight);
                System.out.println("Total score : " + total + "  = \t" + percentage_score +"% from possible maximum 100" );
                System.out.println("Weighted Score is " + weighted_score );
            }

        } else if(ans == 12) {
            System.out.println("Exit to main menu");
            show_menu = "";
        }
    }

    static void show_written_works_menu(){
        System.out.println("                         Select Written Works to put Score:            ");
        System.out.println("|---------------------------------------------------------------------|");
        System.out.println("|1-WW1   3-WW3       5-WW5      7-WW7     9-WW9      11- Score        |");
        System.out.println("|2-WW2   4-WW4       6-WW6      8-WW98   10-WW10     12- Main Menu    |");
        System.out.println("|---------------------------------------------------------------------|");

        ans = input.nextInt ();

        double weight = 0.2;
        process_menu(written_works, "Written works", ans, weight);
    }


    static void show_performance_task_menu() {
        System.out.println("                  Select Performance Task to put Score:     ");
        System.out.println("|------------------------------------------------------------------|");
        System.out.println("|1-PT1   3-PT3      5-PT5       7-PT7    9-PT9       11-Score      |");
        System.out.println("|2-PT2   4-PT4      6-PT6       8-PT8   10-PT10      12-Main Menu  |");
        System.out.println("|------------------------------------------------------------------|");

        ans = input.nextInt();

        double weight = 0.3;
        process_menu(performance_tasks, "Performance task", ans, weight);
    }

    static int get_qa_score_percentage(int score) {
        switch(score) {
            case 30:
                return 100;
            case 29:
                return 98;
            case 28:
                return 96;
            case 27:
                return 94;
            case 26:
                return 92;
            case 25:
                return 90;
            case 24:
                return 88;
            case 23:
                return 86;
            case 22:
                return 84;
            case 21:
                return 82;
            case 20:
                return 80;
            default:
                return 79;
        }
    }

    static void show_quarterly_assessment_menu(){
        System.out.println("                Select Menu                                      ");
        System.out.println("|---------------------------------------------------------------------|");
        System.out.println("|1-Quarterly Assessment  2-Quarterly Assessment score   3-Main Menu   |");
        System.out.println("|---------------------------------------------------------------------|");

        ans = input.nextInt();

        switch (ans) {
            case 1:
                if(qa_score != 0) {
                    System.out.println("Quarterly score " + qa_score + " already added");
                } else  {
                    do {
                        System.out.println("Please input score for quarterly assessment");
                        score = input.nextInt();
                        if(score > 1 && score <= 30) {
                            qa_score = score;
                            System.out.println("Score " + qa_score + " added to quarterly assessment");
                        } else {
                            System.out.println("Please input 1-30 score only");
                        }
                    } while(score > 31);
                }
                break;
            case 2:
                if (qa_score == 0) {
                    System.out.println("QA score not added. Please add it");
                } else  {
                    System.out.println("QA score is " + qa_score);
                    int qa_score_percentage = get_qa_score_percentage(qa_score);
                    System.out.println("Score % is: " + qa_score_percentage);
                }
                break;
            case 3:
                System.out.println("Exit to main menu");
                show_menu = "";
                break;
            default:
                System.out.println("Invalid option chosen. Choose 1-3");
        }
    }

    static void show_final_score() {
        int map_count = 10;
        if(written_works.size() < map_count) {
            System.out.println("Written works are still missing");
        } else if(performance_tasks.size() < map_count) {
            System.out.println("Performance tasks are still missing");
        } else if (qa_score == 0) {
            System.out.println("Quarterly score is still missing");
        } else {
            int written_works_total = get_total_assessment_map_score(written_works);
            double written_works_percentage_score = (double) written_works_total;
            double written_works_weighted_score = (written_works_percentage_score * 0.2);


            int performance_tasks_total = get_total_assessment_map_score(performance_tasks);
            double performance_tasks_percentage_score = (double) performance_tasks_total;
            double performance_tasks_weighted_score = (performance_tasks_percentage_score * 0.3);

            int qa_score_percentage = get_qa_score_percentage(qa_score);
            double qa_score_weighted = (qa_score_percentage * 0.5);

            System.out.format("written %f perf %f qa %f", written_works_weighted_score, performance_tasks_weighted_score, qa_score_weighted);

            double total_score = written_works_weighted_score + performance_tasks_weighted_score + qa_score_weighted;
            System.out.println("Final score is: " + total_score);
        }
        show_menu = "";
    }
}